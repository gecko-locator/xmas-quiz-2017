<html>
<head>

<title>Gecko Locator's Christmas Quiz 2017</title>
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">

<style>

body {
    font-family: "Roboto", sans-serif;
    min-width: 1000px;
    margin: 25px;
}

ul,li {
    margin: 0;
    padding: 0;
}

li {
    list-style: none;
    float: left;
    display: inline;
    margin-right: 10px;
}

.q {
    width: 150px;
    height: 113px;
}

.q img {
    max-width: 100%;
    max-height: 100%;
    height: auto;
}

ul p {
    text-align: center;
    margin: 10px;
    margin-bottom: 40px;
}

.picline {
    overflow: auto;
    width: 90%;
    margin-left: auto;
    margin-right: auto;
    text-align: center;
}

#preview {
    position: absolute;
}

.instructions {
    margin: 20px 0 50px 0;
}

</style>

<script src="jquery.js"></script>

<script type=text/javascript>
this.imagePreview = function () {
    xOffset = 10;
    yOffset = 30;

    $("a.preview").hover(function(e){
        this.t = this.title;
        this.title = "";
        var c = (this.t != "") ? "<br/>" + this.t : "";
        $("body").append("<p id='preview'><img width=300 src='" + this.href + "' alt='Image preview' />" + c + "</p>");
        $("#preview").css("top", (e.pageY - xOffset) + "px")
                     .css("left", (e.pageX - yOffset) + "px")
                     .fadeIn("fast");
    },
    function () {
        this.title = this.t;
        $("#preview").remove();
    });

    $("a.preview").mousemove(function(e){
        $("preview").css("top", (e.pageY - xOffset) + "px")
                    .css("left", (e.pageX - yOffset) + "px");
    });
};

$(document).ready(function(){ imagePreview(); });
</script>

<?
$imgs = [
    [
        "59.jpg",
        "151.jpg",
        "61.jpg",
        "25.jpg",
        "181.jpg"
    ],
    [
        "139.jpg",
        "16.jpg",
        "7.jpg",
        "3.jpg",
        "13.jpg",
    ],
    [
        "5.jpg",
        "173.jpg",
        "83.jpg",
        "9.jpg",
        "131.jpg"
    ],
    [
        "https://upload.wikimedia.org/wikipedia/commons/6/64/%E0%B4%87%E0%B4%9E%E0%B5%8D%E0%B4%9A%E0%B4%BF.JPG",
        "https://upload.wikimedia.org/wikipedia/commons/f/fa/Carl_Fr%C3%B6schl_S%C3%BCsse_Trauben.jpg",
        "71.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/0/0b/AgrihanNASA.jpg",
        "89.jpg"
    ],
    [
        "https://upload.wikimedia.org/wikipedia/commons/0/02/Australian_Made_CMI_H2D_Home_Safe.JPG",
        "https://upload.wikimedia.org/wikipedia/commons/0/0e/Marian_and_Vivian_Brown.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/3/34/Marin_mersenne.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/8/8b/Germain.jpeg",
        "https://upload.wikimedia.org/wikipedia/commons/6/6e/Football_%28soccer_ball%29.svg"
    ],
    [
        "https://upload.wikimedia.org/wikipedia/commons/2/25/Luke_Donald_2.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/9/97/Barb_gonio_080525_9610_ltn_Cf.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/4/48/Studio_publicity_Kirk_Douglas.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/d/d9/RoyalSocMace20040420.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/9/9d/Atlantic_mobula_lisbon.jpg"
    ],
    [
        "https://upload.wikimedia.org/wikipedia/commons/d/d8/Donald_Trump_in_Manchester_NH_February_8%2C_2016_Cropped.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/5/5a/Skateboard_facility_at_Guantanamo.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/9/9d/Sr-71suit.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/b/b9/Rainbow_Trout.jpg",
        "81.jpg"
    ],
    [
        "163.jpg",
        "109.jpg",
        "43.jpg",
        "103.jpg",
        "107.jpg"
    ],
    [
        "https://upload.wikimedia.org/wikipedia/commons/5/53/Liasis_mackloti_savuensis_2.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/4/47/Croce-Mozart-Detail.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/9/98/Aksara_Jawa_-_basa.svg",
        "49.jpg",
        "https://upload.wikimedia.org/wikipedia/commons/thumb/0/02/Music_C_sharp.svg/1024px-Music_C_sharp.svg.png"
    ],
    [
        "4.jpg",
        "67.jpg",
        "19.jpg",
        "169.jpg",
        "121.jpg"
    ]
];

$nums = [
    [59, 151, 61, 25, 181], 
    [139, 16, 7, 3, 13], 
    [5, 173, 83, 9, 131], 
    [113, 11, 71, 17, 89], 
    [167, 53, 41, 79, 37], 
    [149, 137, 2, 157, 97], 
    [73, 47, 29, 31, 81], 
    [163, 109, 43, 103, 107], 
    [179, 23, 101, 49, 127], 
    [4, 67, 19, 169, 121]
];

?>

</head>

<body>
<h1>Gecko Locator's Christmas Quiz 2017</h1>
<div class="instructions">
<p>
Four of the five pictures in each row below have a theme or quality in common. 
</p>

<p>
Your task is to find the odd picture out in each row, then multiply the ten corresponding
numbers together to get a final answer.
</p>

<p>
You must provide me with the <b>exact</b> product of the ten numbers, otherwise I won't
be able to work out what your answers were. If you're struggling to find a calculator
that can do this, try typing the product into 
<a href="https://www.wolframalpha.com/input/?i=1+*+2+*+3+*+4+*+5+*+6+*+7+*+8+*+9+*+10">
Wolfram Alpha</a>.
</p>

<p>
Working out the connections may require a wide range of knowledge, so feel free to team up with colleagues if you are happy to share your prize...
</p>
</div>

<? for ($i=0; $i<count($imgs); $i++){ ?>
<div class="picline">
<ul>
  <? for ($j=0; $j<5; $j++) { ?>
  <li>
    <div>
      <div class="q">
        <a href=<? echo $imgs[$i][$j]; ?> class="preview"><img src="<? echo $imgs[$i][$j];?>"/></a>
      </div>
      <p><? echo $nums[$i][$j];?></p>
    </div>
  </li>
  <? } ?>
</ul>
</div>
<? } ?>

<div class="instructions">
<p>
Answers should be emailed <a href="mailto:matt.ferguson@darktrace.com">here</a>.
The first correct answer in my inbox will win. If nobody gets all ten correct, then the 
winner will be the entrant with the most questions correct in the quickest time.
</p>
</div>

</body>
</html>
